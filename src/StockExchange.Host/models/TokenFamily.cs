namespace StockExchange.Host.models
{
    public interface ITokenFamily
    {
        string AuthenticationToken { get; init; }
        string RefreshToken { get; init; }
    }

    public class TokenFamily : ITokenFamily
    {
        public string AuthenticationToken { get; init; }
        public string RefreshToken { get; init; }
    }
}